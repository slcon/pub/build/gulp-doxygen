/*****************************************************************************************
* gulp-doxygen
*****************************************************************************************/
(function() { 'use strict';

const Q      = require('q');
const common = require('@slcpub/gulp-common');
const $self  = module.exports = common.makeModule(__dirname);

/*---------*/
/* Options */
/*---------*/
$self.options = Object.defineProperties(
  {
  get DIR_DOXY_HTML()   { return `${this.docsDir}/html`; },
  get DIR_DOXY_XML()    { return `${this.docsDir}/xml`; },
  get FP_INDEX_XML()    { return `${this.DIR_DOXY_XML}/index.xml`; },
  get FP_COMBINE_XSLT() { return `${this.DIR_DOXY_XML}/combine.xslt`; },
  get FP_SENSE_XSLT()   { return `${__dirname}/assets/BuildIntellisense.xslt`; },
  },
  Object.assign(
    {
    /*----------*/
    /* Defaults */
    /*----------*/
    defaults: { value: 
      {
      doxyDir:     './',
      doxyFile:    'Doxygen.conf',
      doxyBinPath: 'C:\\Program Files\\Doxygen\\bin',
      docsDir:     './docs',
      senseDir:    './docs',
      nodeBinPath: '.\\node_modules\\.bin'
      }},
    
    /*---------------------------------------------*/
    /* options.reset() - Reset to default options. */
    /*---------------------------------------------*/
    reset: { value: function() { return Object.assign(this, this.defaults); }}
    },

    /*-----------------------------------------------------*/
    /* Path properties automatically update PATH variable. */
    /*-----------------------------------------------------*/
    common.makePathProperty('doxyBinPath'),
    common.makePathProperty('nodeBinPath')
    )
  ).reset();

/*--------------*/
/* Simple Tasks */
/*--------------*/
Object.assign($self.tasks, 
  {
  /**
  * Builds Doxygen HTML output.
  *
  * @task  {build-docs}
  * @group {Supporting Tasks}
  * @order {200}
  */
  'build-docs': () => common.rmdir($self.options.DIR_DOXY_HTML).then(() => $self.run({ GENERATE_HTML:'YES' })),

  /**
  * Removes Doxygen output folder.
  *
  * @task  {clean-docs}
  * @group {Supporting Tasks}
  * @order {200}
  */
  'clean-docs': () => Q.all([common.rmdir($self.options.docsDir), common.rmdir($self.options.senseDir)]),
  });
})();
