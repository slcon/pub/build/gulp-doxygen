/*****************************************************************************************
* Task: build-sense */
/**
* Builds Intellisense XML file from Doxygen XML output into the Doxygen project output
* folder.
*
* @task  {build-sense}
* @group {Supporting Tasks}
* @order {200}
*****************************************************************************************/
(function() { 'use strict';

const Q       = require('q');
const common  = require('@slcpub/gulp-common');
const doxygen = require('../index.js');

module.exports = (() =>
  {
  var pkgInfo         = common.getPackageInfo();
  var senseDir        = doxygen.options.senseDir;
  var outputFile      = `${senseDir}/${pkgInfo.name}.xml`;
  var combinedXML     = `${doxygen.options.docsDir}/combined.xml`;
  var DIR_DOXY_XML    = doxygen.options.DIR_DOXY_XML;
  var FP_INDEX_XML    = doxygen.options.FP_INDEX_XML;
  var FP_COMBINE_XSLT = doxygen.options.FP_COMBINE_XSLT;
  var FP_SENSE_XSLT   = doxygen.options.FP_SENSE_XSLT;

  return Q.all([common.rmdir(DIR_DOXY_XML), common.mkdir(senseDir), common.rm(outputFile)])
  .then(() => doxygen.run({ GENERATE_XML:'YES' }))
  .then(() => common.run(`xslt3 -s:"${FP_INDEX_XML}" -xsl:"${FP_COMBINE_XSLT}" -o:"${combinedXML}"`))
  .then(() => common.run(`xslt3 -s:"${combinedXML}"  -xsl:"${FP_SENSE_XSLT}"   -o:"${outputFile}"`))
  .then(() => common.rm(combinedXML));
  })
})();

