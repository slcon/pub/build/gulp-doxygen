/*****************************************************************************************
* run */
/**
* Run Doxygen to create documentation. Runs Doxygen in doc project folder because config 
* file paths are relative to `cwd`.
* 
* @param  {object}  vars  Environment variables to set.
* @return {Promise} Resolved if successful, rejected with fatal error.
*****************************************************************************************/
(function() { 'use strict';

const common = require('@slcpub/gulp-common');

module.exports = (doxygen) => ((vars) =>
  {
  const doxyDir   = doxygen.options.doxyDir;
  const docsDir = doxygen.options.docsDir;
  const doxyFile  = doxygen.options.doxyFile;

  const opts = Object.entries(vars||{}).map(e => `SET ${e[0]}=${e[1]}`).join(' && ');
  const cmd  = (opts ? `${opts} && ` : '')+`doxygen ${doxyFile}`;
  return common.mkdir(docsDir).then(() => common.run(`cmd /c "${cmd}"`, { cwd:doxyDir }));
  })
})();
